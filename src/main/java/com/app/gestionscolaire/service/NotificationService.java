package com.app.gestionscolaire.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.gestionscolaire.entity.Admission;
import com.app.gestionscolaire.entity.Notification;
import com.app.gestionscolaire.repository.NotificationRepository;

@Service
public class NotificationService {
	
	@Autowired
	private NotificationRepository repository;
	
	public Notification SaveOneNotification(Notification Notification) {
		return repository.save(Notification);
	}
	
	
	public List<Notification> SaveManyNotification(List<Notification> notifications){
		return repository.saveAll(notifications);
	}

	public Notification GetNotificationById(Long id) {
		return repository.findById(id).orElse(null);
	}
	
	public List<Notification> GetAllNotification(){
		return repository.findAll();
	}
	
	public Notification UpdateNotification(Notification notification) {
		Notification noti = repository.findById(notification.getIdNotification()).orElse(null);
		noti.setAdresseDepart(notification.getAdresseDepart());
		noti.setAdresseArrive(notification.getAdresseArrive());
		noti.setContenu(notification.getContenu());
		noti.setDate(notification.getDate());
		return noti;
	}
	
	public String DeleteNotification(Long id) {
		repository.deleteById(id);
		return "Suppression avec succes";
	}
}
