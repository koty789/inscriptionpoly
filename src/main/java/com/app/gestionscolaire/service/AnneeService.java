package com.app.gestionscolaire.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.gestionscolaire.entity.Annee;
import com.app.gestionscolaire.repository.AnneeRepository;

@Service
public class AnneeService {
	
	@Autowired
	private AnneeRepository repository;

	public Annee SaveAnne(Annee annee) {
		return repository.save(annee);
	}
	
	
	public List<Annee> GetAllAnnee(){
		return repository.findAll();
	}
	
	public Annee GetOneAnne(int id) {
		return repository.findById(id).orElse(null);
	}
	
	
	public String DeleteAnnee(int id) {
		repository.deleteById(id);
		return "Annee Spprimer";
	}
	
	public Annee UpdateAnnee(Annee annee) {
		Annee an = repository.findById(annee.getIdAnne()).orElse(null);
		an.setNiveau(annee.getNiveau());
		
		return repository.save(an);
	}
	
}
