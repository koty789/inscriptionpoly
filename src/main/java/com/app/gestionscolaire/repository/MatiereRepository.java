package com.app.gestionscolaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.gestionscolaire.entity.Matiere;

public interface MatiereRepository extends JpaRepository<Matiere, Integer>{

}
