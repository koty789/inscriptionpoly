package com.app.gestionscolaire.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;



@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class Users 
{
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long iduser;
	private String password;
	@Column(unique=true,nullable = false)
	private String username;
	private Boolean active;
	@NotEmpty
	@Column(unique=true,nullable = false)
	@Email
	private String email;
	@Column(nullable = false,length = 20)
	private String nom;
	@Column(nullable = false,length = 20)
	private String prenom;	
	private String photo;

	@ManyToMany(fetch = FetchType.EAGER)
	Set<Role> roles=new HashSet<>();
	
	@Column(name = "verification_code",updatable = false)
	private String verification_code;
	
	private String confirmationToken;

	
	public Long getIduser() {
		return iduser;
	}
	public void setIduser(Long iduser) {
		this.iduser = iduser;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public Users() {
		super();
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Users(Long iduser, String password, Boolean active, @Email String email) {
		this.iduser = iduser;
		this.password = password;
		
		this.active = active;
		this.email = email;
	}

	public String getVerification_code() {
		return verification_code;
	}

	public void setVerification_code(String verification_code) {
		this.verification_code = verification_code;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getConfirmationToken() {
		return confirmationToken;
	}
	public void setConfirmationToken(String confirmationToken) {
		this.confirmationToken = confirmationToken;
	}

	
	
	

}
