package com.app.gestionscolaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.app.gestionscolaire.entity.Users;

public interface UserRepository extends JpaRepository<Users, Long> 
{
	@Query("Select u from Users u WHERE u.username=:username")
	public Users findByUsername(String username);

	@Query("SELECT u from Users u WHERE u.verification_code = ?1")
	public Users findByVerification_code(String code);

	@Query("UPDATE Users u SET u.active= true WHERE u.iduser= ?1")
	@Modifying
	public void active(Long id);
	@Query("Select u from Users u WHERE u.email=:email")
	Users findByEmail(String email);
	
	@Query("Select u from Users u WHERE u.confirmationToken=:confirmationToken")
    Users findByConfirmationToken(String confirmationToken);
	
	
}
