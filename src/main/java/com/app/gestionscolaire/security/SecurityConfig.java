package com.app.gestionscolaire.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;




@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter

{
	//On retourne là un objet de type encoder qui sera utilisé pour encoder le motdepasse 
			//PasswordEncoder passwordEncoder=passwordEncoder();
			//In memory Authen 
			/*auth.inMemoryAuthentication().withUser("user1").password(passwordEncoder.encode("1234")).roles("USER");
			auth.inMemoryAuthentication().withUser("admin").password(passwordEncoder.encode("1234")).roles("ADMIN");
			*/
			
			//JDBC auth
			/*auth.jdbcAuthentication()
				.dataSource(datasource)
				.usersByUsernameQuery("Select username as principal , password as credentials , active from user where username=?")
				.authoritiesByUsernameQuery("Select username as principal ,role as role from user_role where username=?")
				.passwordEncoder(passwordEncoder)
				.rolePrefix("ROLE_");*/
			//UserdeatailsServiceImpl
					
			//auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
			
	/*@Autowired
	private DataSource datasource;*/
	
		
	 @Autowired
	 UserDetailsService userDetailsService;
	 @Autowired
	 private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.userDetailsService(userDetailsService)
			.passwordEncoder(bCryptPasswordEncoder);
		
		
		
		
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception 
	{
		//http.httpBasic();//Pour un formulaire d'authentification basic
		http.formLogin().loginPage("/login");
		http.authorizeRequests().antMatchers("/admin/**").hasRole("ADMIN");
		http.authorizeRequests().antMatchers("/","/**","/login","/webjars/**").permitAll();//Permet d'autoriser les ressources et certaines pages sans auth
		
		//http.authorizeRequests().anyRequest().authenticated();//Toutes les réquetes sont bloqués à condt de s'auth,en la commentant certaines pages non protégées seront accessibles 
		//Il vaut mieux la decommenter et liberer la page que vous souhaitez ouvrir à tout le monde
		
		http.exceptionHandling().accessDeniedPage("/notAutorized");//Page d'erreur 
	    
	}
	//Ce objet pourra être injecter dans les autres parties de l'appli en ajoutant le bean
	
	
	@Bean
	public BCryptPasswordEncoder getEncoder() {
		return new BCryptPasswordEncoder();
	}
}
