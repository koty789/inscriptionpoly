package com.app.gestionscolaire.webController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.app.gestionscolaire.entity.Document;
import com.app.gestionscolaire.service.DocumentService;


@Controller
public class DocumentControllerWeb {
	
	@Autowired
	private DocumentService service;
	
	
	@RequestMapping("/document/list")
	public String ListDocument(Model model) {
		List<Document> documents = service.GetAllDocument();
		model.addAttribute("documents", documents);
		return "listdoc";
	}
	
	@RequestMapping("/document/new")
	public String NewDocument(Model model) {
		Document doc = new Document();
		model.addAttribute("document", doc);
		return "newdocument";

	}
	
	@RequestMapping("document/save")
	public String SaveDocument(Model model, @ModelAttribute("document") Document document) {
		service.SaveDocument(document);
		
		return "redirect:/document/list";
	}
	
	@RequestMapping(value="document/delete", method = RequestMethod.POST)
	public String DeleteDocument(Model model, @PathVariable("id") Long id) {
		service.DeleteDocument(id);
		return "redirect:/listdoc";
	}
	
	@PostMapping("document/edit/{id}")
	public String EditDocument(Model model, @PathVariable("id") Long id) {
		Document document= service.GetDocumentById(id);
		model.addAttribute("Document", document);
		return "redirect:/document/editDocument";
	}
	
	@RequestMapping("document/edition")
	public String modifDocument(@ModelAttribute("Document") Document document) {
		
		 return "redirect:/document/AllDocument";
	}
	

}
