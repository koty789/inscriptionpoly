package com.app.gestionscolaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.gestionscolaire.entity.Role;

public interface RoleRepository  extends  JpaRepository<Role, Long> 

{
	public Role findByRoleName(String roleName);

}
