package com.app.gestionscolaire.webController;


import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Set;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.app.gestionscolaire.entity.Candidat;

import com.app.gestionscolaire.entity.Users;

import com.app.gestionscolaire.service.AccountService;
import com.app.gestionscolaire.service.CandidatService;
import com.app.gestionscolaire.service.UserDetailsServiceImpl;
import com.app.gestionscolaire.service.Utility;


@Controller
public class AdminControllerWeb

{
	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;
	@Autowired
	private AccountService actservice;
	@Autowired
	private CandidatService candidatService;

	@RequestMapping("/notAutorized")
	public String error() {
		return "notAutorized";
	}

	@RequestMapping("/")
	public String index(Model model,HttpServletRequest request) 
	{
		return "registration2";
	}

	@RequestMapping("/login")
	public String login() {
		return "login2";
	}
	@RequestMapping("loginUser")
	public String loginUser(@ModelAttribute("user") Users user, ModelMap mod,HttpServletRequest request)
	{
		Users userExist=actservice.findByEmail(user.getEmail());
		
		if(userExist==null)
		{
			mod.addAttribute("userNotExist", "Adresse email ou mot de passe incorrect !");
			return "redirect:/login";
		}
		else
		{
			  
			  UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername(userExist.getUsername());
	
	          UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(), userDetails.getAuthorities());
	
	          SecurityContextHolder.getContext().setAuthentication(authentication);
	
	          request.getSession().setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());		
	          
	          return "test";
		}	
	}
	
	
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request) throws ServletException {
		request.logout();
		return "redirect:/login";
	}

	@RequestMapping(value="/addUser")
	public String addnewuser(ModelMap mod)
	{
		Users user=new Users();
		mod.addAttribute("user", user);		
		return "registrationUser";
	}	

	@RequestMapping(value="/registrationUser", method = RequestMethod.POST)
	public String adduser(ModelMap mod, @Valid Users user, BindingResult bindingResult, HttpServletRequest request) throws UnsupportedEncodingException, MessagingException {
		
		// Lookup user in database by e-mail
		Users userExists = actservice.findByEmail(user.getEmail());
		
		System.out.println(userExists);
		
		if (userExists != null) {
			mod.addAttribute("userExistant", "Oups!  Il existe déjà un utilisateur avec cet adresse mail.");
			bindingResult.reject("email");
		}
			
		if (bindingResult.hasErrors()) 
		{ 
			return addnewuser(mod);		
		} 
		else 
		{ // new user so we create user and send confirmation e-mail
					
			// Disable user until they click on confirmation link in email
		    user.setActive(false);
		   
		    LocalDate today = LocalDate.now();
		    String user_vcode= today.toString();
		    
			user.setConfirmationToken(user_vcode);
		    Users userSave= actservice.saveUser(user);
		    
		    String appUrl = request.getScheme() + "://" + request.getServerName()+":"+request.getServerPort();
		    actservice.sendVerificationEmail(userSave, appUrl); 
		    mod.addAttribute("inscrip", "Inscription réussie! Un mail de validation vous a été envoyé sur votre adresse mail.");
		    return addnewuser(mod);	
			
		}	 
		
	}
	
	
	// Process confirmation link
		@RequestMapping(value="/confirm", method = RequestMethod.GET)
		public String confirmRegistration(ModelMap mod, @RequestParam("token") String token) throws ParseException {
				
			Users user = actservice.findByConfirmationToken(token);
			System.out.println(user+"++");
				
			if (user == null) { // No token found in DB
				mod.addAttribute("invalidToken", "Votre compte a déjà été validé");
				return addnewuser(mod);
			} 
			else 
			{ 				
				// Token found
				Boolean verified = actservice.verify(token);
				if(verified)
				{
					user.setConfirmationToken(null);
					user.setActive(true);
					actservice.saveUser(user);
					return "index";
				}
				else
				{
					mod.addAttribute("valdate", "La durée de vérification de votre inscription est expirée , veuillez vous inscrire à nouveau");
					return addnewuser(mod);
				}
								
				
				 
				 
			}
				
					
		}
	
	@RequestMapping("/registration")
	public String saveUser(@ModelAttribute("user") Users user, HttpServletRequest request)
			throws UnsupportedEncodingException, MessagingException
	{	
		Users newUser= actservice.saveUser(user);
		//actservice.addRoletoUser(newUser.getUsername(),"USER");
		 
		Candidat candidat =new Candidat(newUser.getIduser(), newUser.getPassword(),  newUser.getActive(), newUser.getEmail());
		candidat=candidatService.saveCandidat(candidat);
		String siteURL= Utility.getSiteUrl(request);
		actservice.sendVerificationEmail(newUser,siteURL);
		return "successReg";
	}
	
}
