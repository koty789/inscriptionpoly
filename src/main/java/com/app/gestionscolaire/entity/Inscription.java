package com.app.gestionscolaire.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity(name="inscription")
public class Inscription {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idInscription")
	private Long idInscription;
	
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="date")
	private Date date;
	
	@Column(name="etat")
	private int etat;
	
	
	@OneToMany(mappedBy="inscriptionDoc")
	private Set<Document> documents = new HashSet<Document>();
	
	@OneToMany(mappedBy="inscriptionAd")
	private Set<Admission> admissions =new HashSet<Admission>();
	
	@OneToMany(mappedBy="inscriptionSuiv")
	private Set<Suivis> suivis = new HashSet<Suivis>();
	
	
		public Inscription() {
	
			}
	
	
	public Inscription(Long idInscription, Date date, int etat) {
		super();
		this.idInscription = idInscription;
		this.date = date;
		this.etat = etat;
	}

	public Long getIdInscription() {
		return idInscription;
	}

	public void setIdInscription(Long idInscription) {
		this.idInscription = idInscription;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}
	
	

}
