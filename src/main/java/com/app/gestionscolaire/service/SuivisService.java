package com.app.gestionscolaire.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.gestionscolaire.entity.Suivis;
import com.app.gestionscolaire.repository.SuivisRepository;

@Service
public class SuivisService {
	
	@Autowired
	private SuivisRepository repository;
	
	public Suivis SaveOneSuivis(Suivis Suivis) {
		return repository.save(Suivis);
	}
	
	
	public List<Suivis> SaveManySuivis(List<Suivis> Suiviss){
		return repository.saveAll(Suiviss);
	}

	
	public Suivis GetSuivisById(Long id) {
		return repository.findById(id).orElse(null);
	}
	
	public List<Suivis> GetAllSuivis(){
		return repository.findAll();
	}
	
	public Suivis UpdateSuivis(Suivis suivis) {
		Suivis suiv = repository.findById(suivis.getIdSuivis()).orElse(null);
		suiv.setInscriptionSuiv(suivis.getInscriptionSuiv());
		suiv.setNotification(suivis.getNotification());
		suiv.setDate(suivis.getDate());
		suiv.setOperation(suivis.getOperation());
		return repository.save(suiv);
	}
	
	public String DeleteSuivis(Long id) {
		repository.deleteById(id);
		return "Suivis supprimer";
	}
	

}
