package com.app.gestionscolaire.repository;

import com.app.gestionscolaire.entity.Candidat;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface CandidatRepository extends JpaRepository<Candidat,Long> {
    
}
