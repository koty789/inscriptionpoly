package com.app.gestionscolaire.webController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.app.gestionscolaire.entity.Matiere;
import com.app.gestionscolaire.service.MatiereService;

@Controller
public class MatiereControllerWeb {

	@Autowired
	private MatiereService service;
	
	
	@RequestMapping("/matiere/list")
	public String ListMatiere(Model model) {
		List<Matiere> matieres = service.GetAllMatiere();
		model.addAttribute("matieres", matieres);
		return "listmat";
	}
	
	@RequestMapping("/matiere/new")
	public String NewMatiere(Model model) {
		Matiere mat = new Matiere();
		model.addAttribute("matiere", mat);
		return "newmatiere";
	}
	
	@RequestMapping("matiere/save")
	public String SaveMat(Model model, @ModelAttribute("matiere") Matiere matiere) {
		service.SaveMatiere(matiere);
		return "redirect:/matiere/list";
	}
	
	@RequestMapping(value="matiere/delete", method = RequestMethod.POST)
	public String DeleteMatiere(Model model, @PathVariable("id") int id) {
		service.DeleteMatiere(id);
		return "redirect:/matiere/list";
	}
	
	@PostMapping("matiere/edit/{id}")
	public String EditMatiere(Model model, @PathVariable("id") int id) {
		Matiere matiere= service.GetOneByIdMatiere(id);
		model.addAttribute("matiere", matiere);
		return "redirect:/matiere/edition";
	}
	
	@RequestMapping("matiere/edition")
	public String modifMatiere(@ModelAttribute("matiere") Matiere annne) {
		
		 return "redirect:/matiere/list";
	}
	
}
