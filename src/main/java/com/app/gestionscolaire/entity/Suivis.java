package com.app.gestionscolaire.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity(name="suivis")
public class Suivis {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idSuivis")
	private Long idSuivis;
	
	@Column(name="opreration")
	private String operation;
	
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="date")
	private Date date;
	
	//private Employe employe;
	
	@ManyToOne
    @JoinColumn(name="suivis_id", nullable=false)
	private Inscription inscriptionSuiv;
	
	@ManyToOne
    @JoinColumn(name="notification_id", nullable=false)
	private Notification notification;

	public Long getIdSuivis() {
		return idSuivis;
	}

	public void setIdSuivis(Long idSuivis) {
		this.idSuivis = idSuivis;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Inscription getInscriptionSuiv() {
		return inscriptionSuiv;
	}

	public void setInscriptionSuiv(Inscription inscriptionSuiv) {
		this.inscriptionSuiv = inscriptionSuiv;
	}

	public Notification getNotification() {
		return notification;
	}

	public void setNotification(Notification notification) {
		this.notification = notification;
	}

	
}
