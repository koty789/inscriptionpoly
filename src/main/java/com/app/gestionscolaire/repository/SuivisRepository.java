package com.app.gestionscolaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.gestionscolaire.entity.Suivis;

public interface SuivisRepository  extends JpaRepository<Suivis, Long>{

}
