package com.app.gestionscolaire.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.gestionscolaire.entity.Matiere;
import com.app.gestionscolaire.repository.MatiereRepository;

@Service
public class MatiereService {
	
	@Autowired
	private MatiereRepository repository;
	

	public Matiere SaveMatiere(Matiere matiere) {
		return repository.save(matiere);
	}
	
	public List<Matiere> SaveAllMatiere(List<Matiere> matieres){
		return repository.saveAll(matieres);
	}
	
	public List<Matiere> GetAllMatiere(){
		return repository.findAll();
	}
	
	public Matiere GetOneByIdMatiere(int id) {
		return repository.findById(id).orElse(null);
	}
	
	public String DeleteMatiere(int id) {
		repository.deleteById(id);
		return "Matiere supprimer";
	}
	
	public Matiere UpdateMatiere(Matiere matiere) {
		Matiere mati = repository.findById(matiere.getIdMatiere()).orElse(null);
		mati.setName(matiere.getName());
		 return repository.save(mati);
	}
}
