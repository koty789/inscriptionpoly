package com.app.gestionscolaire.webController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.gestionscolaire.entity.Admission;
import com.app.gestionscolaire.entity.Inscription;
import com.app.gestionscolaire.service.AdmissionService;
import com.app.gestionscolaire.service.NotificationService;

@Controller
public class AdmissionControllerWeb {
	
	@Autowired
	private AdmissionService service;
	
	//@Autowired
	//private Inscription inscriptService;

	@RequestMapping("/admission/list")
	public String ListDocument(Model model) {
		List<Admission> admissions = service.GetAllAdmission();
		model.addAttribute("admissions", admissions);
		return "listAdmission";
	}
	
	
	@RequestMapping("/admission/new")
	public String NewAdmission(Model model) {
		Admission ad = new Admission();
		model.addAttribute("admission", ad);
		//model.addAttribute("notificationlist", notifService.GetAllNotification());
		return "newAdmission";
	}
	
	
	@RequestMapping("admission/save")
	public String SaveInscrp(Model model, @ModelAttribute("admission") Admission admission) {
		service.SaveOneAdmission(admission);
		return "redirect:/admission/list";
	}
	
	@GetMapping("admission/edit/{id}")
	public String EditerAdmission(Model model, @PathVariable("id") Long id) {
		Admission admission = service.GetAdmissionById(id);
		model.addAttribute("admission", admission);
		return "redirect:/admission/modifAdmission";
	}
	
	
	@PostMapping("admission/edition")
	public String EditAdmission(@ModelAttribute("admission") Admission admission) {
		return "redirect:/admission/editAdmission";
	}
	
	@RequestMapping("admission/delete")
	public String SupprimerAdmission(Model model, @ModelAttribute("id") Long id) {
		service.DeleteAdmission(id);
		return "redirect:/admission/ListAdmission";
	}

}
