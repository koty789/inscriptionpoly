package com.app.gestionscolaire.repository;

import com.app.gestionscolaire.entity.Employe;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface EmployeRepository extends JpaRepository<Employe ,Long> {
    
}
