package com.app.gestionscolaire.service;

import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.util.HashSet;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.app.gestionscolaire.entity.Role;
import com.app.gestionscolaire.entity.Users;
import com.app.gestionscolaire.repository.RoleRepository;
import com.app.gestionscolaire.repository.UserRepository;

@Service
@Transactional
public class AccountService

{
	@Autowired
	private EmailService emailService;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private RoleRepository roleRepo;
	@Autowired
	private JavaMailSender mailSender;
	//Save User
	public Users saveUser(Users user) {
		String passEncryp = bCryptPasswordEncoder.encode(user.getPassword());
		user.setPassword(passEncryp);
		user.setRoles(new HashSet<>(roleRepo.findAll()));
		return userRepo.save(user);

	}
	//find user by username
	public Users findUserByUsername(String username) {
		return userRepo.findByUsername(username);
	}
	//get list of users
	
	public List<Users> findUsers() {
		return userRepo.findAll();
	}
	
	public Role saveRole(Role role) {
		return roleRepo.save(role);
	}
	//find user by verification_code
	public Users findByVerification_code(String code)
	{
		return userRepo.findByVerification_code(code);
	}
	//find user by mail
	
	public Users findByEmail(String email)
	{
		return userRepo.findByEmail(email);
	}
	
	
	
	public Role findRoleByRolename(String rolename) {
		return roleRepo.findByRoleName(rolename);
	}

	public void addRoletoUser(String username, String roleName) {
		Role role = roleRepo.findByRoleName(roleName);
		Users user = userRepo.findByUsername(username);
		user.getRoles().add(role);
	}

	

	
	public void sendVerificationEmail(Users user, String siteURL) throws UnsupportedEncodingException,MessagingException
			
	{
		/*String subject="Verifiez s'il vous plait votre inscription";
		String senderName="Institut Polytechnique Privé de Casablanca";
		String mailContent= "<p> Cliquez s'il vous plait sur lle lien en dessous pour valider votre inscription:</p>";
		
		String verifyURL=siteURL+ "/verify?code="+user.getVerification_code();

		mailContent += "<h3> <a href= \"" + verifyURL + "\"> VERIFIEZ</a></h3>";
		
		mailContent+="<p> Merci <br> Institut Polytechnique Privé de Casablanca</p>";
	
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);

		helper.setFrom("koty7243@fmail.com",senderName);
		helper.setTo(user.getEmail());
		helper.setSubject(subject);

		helper.setText(mailContent, true);

		mailSender.send(message);*/
		
		
		
		SimpleMailMessage registrationEmail = new SimpleMailMessage();
		registrationEmail.setTo(user.getEmail());
		registrationEmail.setSubject("Confirmation de l'inscription");
		registrationEmail.setText("Pour confirmer votre adresse mail, cliquez s'il vous plait sur le lien en bas:\n"
				+ siteURL + "/confirm?token=" + user.getConfirmationToken());
		registrationEmail.setFrom("koty7243@gmail.com");
		
		
		emailService.sendEmail(registrationEmail);
		
	
	}
	
	public Boolean verify(String verification_code) throws ParseException 
	{
		Users user1=userRepo.findByConfirmationToken(verification_code);
		
		 
		String code=user1.getConfirmationToken();
		LocalDate date= LocalDate.parse(code);
		
		Duration dur = Duration.between(date.atStartOfDay(),LocalDate.now().plusDays(1).atStartOfDay());
		Long nbhour=dur.toHours();
		System.out.println(nbhour);
		if(nbhour>=24.0)
		{
			return false;
		}
		else
		{
			userRepo.active(user1.getIduser());
			return true;
		}


	}

	public Users findByConfirmationToken(String token) 
	{
		
		return userRepo.findByConfirmationToken(token);
	}
	
}
