package com.app.gestionscolaire.service;

import com.app.gestionscolaire.entity.Candidat;
import com.app.gestionscolaire.repository.CandidatRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CandidatService 
{
    @Autowired
    private CandidatRepository candidatRepo;

    public Candidat saveCandidat(Candidat candi)
    {
        return candidatRepo.save(candi);
    }
    
}
