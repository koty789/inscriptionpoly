package com.app.gestionscolaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.gestionscolaire.entity.Inscription;

public interface InscriptionRepository extends JpaRepository<Inscription, Long>{

}
