package com.app.gestionscolaire.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.gestionscolaire.entity.Document;
import com.app.gestionscolaire.repository.DocumentRepository;

@Service
public class DocumentService {
	
	@Autowired
	private DocumentRepository repository;
	
	
	public Document SaveDocument(Document doc) {
		return repository.save(doc);
	}
	
	public List<Document> SaveDocument(List<Document> documents){
		return repository.saveAll(documents);
	}
	
	public Document GetDocumentById(Long id) {
		return repository.findById(id).orElse(null);
	}
	
	public List<Document> GetAllDocument(){
		return repository.findAll();
	}
	
	public Document UpadteDocument(Document document) {
		Document doc = repository.findById(document.getIdDocument()).orElse(null);
		doc.setName(document.getName());
		return repository.save(doc);
	}
	
	public String DeleteDocument(Long id) {
		repository.deleteById(id);
		return "Document supprimer avec succes";
	}
	

}
