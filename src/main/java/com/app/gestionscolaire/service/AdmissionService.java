package com.app.gestionscolaire.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.gestionscolaire.entity.Admission;
import com.app.gestionscolaire.repository.AdmissionRepository;

@Service
public class AdmissionService {
	
	@Autowired
	private AdmissionRepository repository;
	
	
	public Admission SaveOneAdmission(Admission admission) {
		return repository.save(admission);
	}
	
	
	public List<Admission> SaveManyAdmission(List<Admission> admissions){
		return repository.saveAll(admissions);
	}

	
	public Admission GetAdmissionById(Long id) {
		return repository.findById(id).orElse(null);
	}
	
	public List<Admission> GetAllAdmission(){
		return repository.findAll();
	}
	
	public Admission UpdateAdmission(Admission admission) {
		Admission admi = repository.findById(admission.getIdAdmission()).orElse(null);
		admi.setEtat(admission.getEtat());
		admi.setInscriptionAd(admission.getInscriptionAd());
		admi.setOperation(admission.getOperation());
		return repository.save(admi);
	}
	
	public String DeleteAdmission(Long id) {
		repository.deleteById(id);
		return "admission supprimer";
	}

}
