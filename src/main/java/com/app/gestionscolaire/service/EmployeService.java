package com.app.gestionscolaire.service;

import com.app.gestionscolaire.entity.Employe;
import com.app.gestionscolaire.repository.EmployeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeService 
{
    @Autowired
    private EmployeRepository empRepo;

    public Employe saveEmploye(Employe emp)
    {
        return empRepo.save(emp);
    }
    
}
