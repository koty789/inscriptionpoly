package com.app.gestionscolaire.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="admission")
public class Admission {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idAdmission")
	private Long idAdmission;
	
	@Column(name="opreation")
	private String operation;
	
	@Column(name="etat")
	private int etat;
	
	//private Employe employe;
	
	@ManyToOne
    @JoinColumn(name="admission_id", nullable=false)
	private Inscription inscriptionAd;
	
	

	public Admission() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Admission(Long idAdmission, String operation, int etat, Inscription inscriptionAd) {
		super();
		this.idAdmission = idAdmission;
		this.operation = operation;
		this.etat = etat;
		this.inscriptionAd = inscriptionAd;
	}

	public Long getIdAdmission() {
		return idAdmission;
	}

	public void setIdAdmission(Long idAdmission) {
		this.idAdmission = idAdmission;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public Inscription getInscriptionAd() {
		return inscriptionAd;
	}

	public void setInscriptionAd(Inscription inscriptionAd) {
		this.inscriptionAd = inscriptionAd;
	}

	
	
}
