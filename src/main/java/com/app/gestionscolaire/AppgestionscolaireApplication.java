package com.app.gestionscolaire;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppgestionscolaireApplication 
{

	public static void main(String[] args) 
	{
		SpringApplication.run(AppgestionscolaireApplication.class, args);
		/*DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		LocalDateTime now = LocalDateTime.now();  
		//Convert ldt to string 
		String t1=dtf.format(now);
		//from string to localdatetime
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(t1, formatter);

		Duration dur = Duration.between(dateTime, dateTime);
		String result = String.format("%d", dur.toHours());
		System.out.println(result);*/

		
	
	}

}
