package com.app.gestionscolaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.gestionscolaire.entity.Document;

public interface DocumentRepository  extends JpaRepository<Document, Long>{

}
